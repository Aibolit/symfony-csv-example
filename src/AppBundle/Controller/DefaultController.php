<?php

namespace AppBundle\Controller;

use AppBundle\Domain\Document;
use AppBundle\Form\Type\DocumentImportType;
use AppBundle\Service\DocumentPresenterFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(DocumentImportType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Document $document */
            $document = $form->getData()->getDocument();
            $file = $document->getFile();
            $importer = $this->get('csv_importer');
            $document->setData($importer->import(file_get_contents($file->getPathname())));
            $encoder = DocumentPresenterFactory::getPresenter($form->getData()->getOutput());

            return $encoder->present($document->getData());
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
