<?php

namespace AppBundle\Domain;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Document
{
    /** @var  array */
    private $data;

    /** @var  UploadedFile */
    private $file;

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param resource $file
     * @return Document
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
