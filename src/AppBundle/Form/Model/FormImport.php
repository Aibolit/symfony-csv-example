<?php

namespace AppBundle\Form\Model;

use AppBundle\Domain\Document;

class FormImport
{
    /** @var  Document */
    private $document;

    /** @var  string */
    private $output;

    /**
     * @return Document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param Document $document
     * @return FormImport
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param string $output
     * @return FormImport
     */
    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

}
