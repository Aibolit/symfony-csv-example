<?php

namespace AppBundle\Form\Type;

use AppBundle\Service\DocumentPresenterFactory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('document', DocumentType::class)
            ->add('output', ChoiceType::class, [
                'choices'           => [
                    'JSON'  => DocumentPresenterFactory::EXPORT_JSON,
                    'XML'   => DocumentPresenterFactory::EXPORT_XML,
                    'Plain' => DocumentPresenterFactory::EXPORT_PLAIN,
                ],
                'choices_as_values' => true,
            ])
            ->add('import', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Form\Model\FormImport',
        ]);
    }
}
