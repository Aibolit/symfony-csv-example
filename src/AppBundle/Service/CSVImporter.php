<?php

namespace AppBundle\Service;

class CSVImporter
{
    /**
     * @param $string
     * @return array
     */
    public function import($string)
    {
        $lines = explode(PHP_EOL, trim($string));
        $array = [];
        $columns = str_getcsv(array_shift($lines));
        foreach ($lines as $line) {
            $arrayRow = [];
            foreach ($columns as $key => $name) {
                $arrayRow[$name] = str_getcsv($line)[$key];
            }
            $array[] = $arrayRow;
        }

        return $array;
    }
}
