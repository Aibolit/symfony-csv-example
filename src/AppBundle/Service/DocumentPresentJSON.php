<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Response;

class DocumentPresentJSON implements DocumentPresenterInterface
{
    /**
     * @param array $data
     * @return Response
     */
    public function present(array $data)
    {
        return new Response(

            json_encode($data), // Simple implementation. JsonEncoder might be used.
            200,
            [
                'Content-type' => 'application/json',
            ]
        );
    }
}
