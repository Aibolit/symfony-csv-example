<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Response;

class DocumentPresentPlain implements DocumentPresenterInterface
{
    /**
     * @param array $data
     * @return Response
     */
    public function present(array $data)
    {
        return new Response(
            var_export($data, false),
            200,
            [
                'Content-type' => 'text/plain',
            ]
        );
    }
}
