<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class DocumentPresentXML implements DocumentPresenterInterface
{
    /**
     * @param array $data
     * @return Response
     */
    public function present(array $data)
    {
        $encoder = new XmlEncoder();

        return new Response(
            $encoder->encode($data, 'xml'),
            200,
            [
                'Content-type' => 'application/xml',
            ]
        );
    }
}
