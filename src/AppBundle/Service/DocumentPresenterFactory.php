<?php

namespace AppBundle\Service;

class DocumentPresenterFactory
{
    const EXPORT_JSON = 'json';
    const EXPORT_XML = 'xml';
    const EXPORT_PLAIN = 'plain';

    /**
     * @param string $format
     * @return DocumentPresenterInterface
     */
    public static function getPresenter($format)
    {
        switch ($format) {
            case self::EXPORT_JSON:
                return new DocumentPresentJSON();
                break;
            case self::EXPORT_XML:
                return new DocumentPresentXML();
                break;
            case self::EXPORT_PLAIN:
                return new DocumentPresentPlain();
                break;
            default:
                return new DocumentPresentJSON();
        }
    }
}
