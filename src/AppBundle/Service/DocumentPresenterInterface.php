<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Response;

interface DocumentPresenterInterface
{
    /**
     * @param array $data
     * @return Response
     */
    public function present(array $data);
}
