<?php

namespace AppBundle\Tests\Controller;

use AppBundle\Service\DocumentPresenterFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertContains('CSV Importing Service', $crawler->filter('#container h1')->text());

        $form = $crawler->selectButton('Import')->form();

        $this->assertNotEmpty($form);
    }

    public function testJSON()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Import')->form();

        $crawler = $client->submit($form, [
            'document_import' => [
                'document' => [
                    'file' => new UploadedFile(__DIR__ . '/../sample.csv', 'sample.csv'),
                ],
                'output'   => DocumentPresenterFactory::EXPORT_JSON,
            ],
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertStringEndsWith('application/json', $client->getInternalResponse()->getHeader('Content-type'));
        $this->assertContains('John Walker', $client->getResponse()->getContent());
    }

    public function testXML()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Import')->form();

        $crawler = $client->submit($form, [
            'document_import' => [
                'document' => [
                    'file' => new UploadedFile(__DIR__ . '/../sample.csv', 'sample.csv'),
                ],
                'output'   => DocumentPresenterFactory::EXPORT_XML,
            ],
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertStringEndsWith('application/xml', $client->getInternalResponse()->getHeader('Content-type'));
        $this->assertContains('John Walker', $client->getResponse()->getContent());
    }

    public function testPlain()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $form = $crawler->selectButton('Import')->form();

        ob_start();
        $crawler = $client->submit($form, [
            'document_import' => [
                'document' => [
                    'file' => new UploadedFile(__DIR__ . '/../sample.csv', 'sample.csv'),
                ],
                'output'   => DocumentPresenterFactory::EXPORT_PLAIN,
            ],
        ]);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertStringStartsWith('text/plain', $client->getInternalResponse()->getHeader('Content-type'));
        $this->assertContains('John Walker', ob_get_clean());
    }
}
